package com.opera.loadfragmenttest;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.TimeUtils;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FragmentManager manager;
    private WebView mWebView;
    private SeekBar mSeekBar;
    private TextView mCurrentTimeView;
    private TextView mDurationView;
    private View mLoadingView;
    private String mTestUrl = "https://www.youtube.com/embed/PUn7ymZ-k1I?enablejsapi=1&widgetid=1";


    VideoFragment mFragment = new VideoFragment();
    Handler mHandler = new Handler(Looper.getMainLooper());
    private String js;
    class JsObject {

        @JavascriptInterface
        public void onAbort() {
            Log.d("xgstag_op", "abort\t当音频/视频的加载已放弃时触发。");
        }

        @JavascriptInterface
        public void onCanPlay() {
            Log.d("xgstag_op", "canplay\t当浏览器可以开始播放音频/视频时触发。");
        }

        @JavascriptInterface
        public void onCanPlayThrough() {
            Log.d("xgstag_op", "canplaythrough\t当浏览器可在不因缓冲而停顿的情况下进行播放时触发。");
        }

        @JavascriptInterface
        public void onDurationChange(long duration) {
            Log.d("xgstag_op", "durationchange\t当音频/视频的时长已更改时触发。 : " + duration);
            mHandler.post(()->{
                mSeekBar.setMax((int) duration);
                mDurationView.setText(String.valueOf(duration) + "S");
            });
        }

        @JavascriptInterface
        public void onEmptied() {
            Log.d("xgstag_op", "emptied\t当目前的播放列表为空时触发。");
        }

        @JavascriptInterface
        public void onEnded() {
            Log.d("xgstag_op", "ended\t当目前的播放列表已结束时触发。");
            mHandler.post(()->{
                Toast.makeText(MainActivity.this, "播放完成", Toast.LENGTH_SHORT).show();
            });
        }

        @JavascriptInterface
        public void onError() {
            Log.d("xgstag_op", "error\t当在音频/视频加载期间发生错误时触发。");
        }

        @JavascriptInterface
        public void onLoadedData() {
            Log.d("xgstag_op", "loadeddata\t当浏览器已加载音频/视频的当前帧时触发。");
        }

        @JavascriptInterface
        public void onLoadedMetaData() {
            Log.d("xgstag_op", "loadedmetadata\t当浏览器已加载音频/视频的元数据时触发。");
        }

        @JavascriptInterface
        public void onLoadStart() {
            Log.d("xgstag_op", "loadstart\t当浏览器开始查找音频/视频时触发。");
        }

        @JavascriptInterface
        public void onPause() {
            Log.d("xgstag_op", "pause\t当音频/视频已暂停时触发。");
        }

        @JavascriptInterface
        public void onPlay() {
            Log.d("xgstag_op", "play\t当音频/视频已开始或不再暂停时触发。");
        }

        @JavascriptInterface
        public void onPlaying() {
            Log.d("xgstag_op", "playing\t当音频/视频在因缓冲而暂停或停止后已就绪时触发。");
        }

        @JavascriptInterface
        public void onProgress() {
            Log.d("xgstag_op", "progress\t当浏览器正在下载音频/视频时触发。");
//            mHandler.post(()->{
//                mLoadingView.setVisibility(View.VISIBLE);
//            });
        }

        @JavascriptInterface
        public void onRatechange() {
            Log.d("xgstag_op", "ratechange\t当音频/视频的播放速度已更改时触发。");
        }

        @JavascriptInterface
        public void onSeeked() {
            Log.d("xgstag_op", "seeked\t当用户已移动/跳跃到音频/视频中的新位置时触发。");
        }

        @JavascriptInterface
        public void onStalled() {
            Log.d("xgstag_op", "stalled\t当浏览器尝试获取媒体数据，但数据不可用时触发。");
        }

        @JavascriptInterface
        public void onSuspend() {
            Log.d("xgstag_op", "suspend\t当浏览器刻意不获取媒体数据时触发。");
        }

        @JavascriptInterface
        public void onTimeUpdate(long t) {
            Log.d("xgstag_op", "timeupdate\t当目前的播放位置已更改时触发。 : " + t);
            mHandler.post(()->{
               mSeekBar.setProgress((int) t);
               mLoadingView.setVisibility(View.GONE);
            });
        }

        @JavascriptInterface
        public void onVolumeChange() {
            Log.d("xgstag_op", "volumechange\t当音量已更改时触发。");
        }

        @JavascriptInterface
        public void onWaiting() {
            Log.d("xgstag_op", "waiting\t当视频由于需要缓冲下一帧而停止时触发。");
            mHandler.post(()->{
                mLoadingView.setVisibility(View.VISIBLE);
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSeekBar = findViewById(R.id.seekbar);
        mCurrentTimeView = findViewById(R.id.current_time);
        mDurationView = findViewById(R.id.duration);
        mLoadingView = findViewById(R.id.loading);
        mWebView = findViewById(R.id.webview);
        WebSettings webSettings = mWebView.getSettings();
        // 使能使用JavaScript
        webSettings.setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(new JsObject(), "OpVideo");
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                js = readAssetFile("b.js");
                Log.d("xgstag_op", js);
                view.loadUrl("javascript:" + js);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }

            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view,
                    WebResourceRequest request) {
                String url = String.valueOf(request.getUrl());
                if (!url.endsWith("base.js")) return super.shouldInterceptRequest(view, request);
                WebResourceResponse response = null;
                response = super.shouldInterceptRequest(view, request);
                try {
                    response = new WebResourceResponse("*", "UTF-8", getAssets().open("a.js"));
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("xgstag_op", "e = " + e);
                }

                return response;
            }
        });
        mWebView.setWebChromeClient(new WebChromeClient() {


        });
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mCurrentTimeView.setText(progress+"S");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                String js = "javascript:window.vcontroller.seekTo(" + seekBar.getProgress() + ");";
                Log.d("xgstag_op", js);
                mWebView.loadUrl(js);
            }
        });

        mWebView.loadUrl(mTestUrl);

        final FrameLayout layoutRed = findViewById(R.id.layout_red);
        final FrameLayout layoutGray = findViewById(R.id.layout_gray);
        manager = this.getSupportFragmentManager();

        layoutRed.setOnClickListener(v -> {
            if (mFragment.isAdded()) {
                reset();
            }
            addFragment(R.id.layout_red);
        });

        layoutGray.setOnClickListener(v -> {
            if (mFragment.isAdded()) {
                reset();
            }
            addFragment(R.id.layout_gray);
        });

        findViewById(R.id.play).setOnClickListener(this);
        findViewById(R.id.pause).setOnClickListener(this);
    }


    private String readAssetFile(String fileName) {
        try {
            AssetManager assetManager =
                    getAssets(); //获得assets资源管理器（assets中的文件无法直接访问，可以使用AssetManager访问）
            InputStreamReader inputStreamReader =
                    new InputStreamReader(assetManager.open(fileName), "UTF-8"); //使用IO流读取json文件内容
            BufferedReader br = new BufferedReader(inputStreamReader);//使用字符高效流
            String line;
            StringBuilder builder = new StringBuilder();

            while (true) {
                if (!((line = br.readLine()) != null)) break;
                builder.append(line);
            }
            br.close();
            inputStreamReader.close();

            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void reset() {
        if (mFragment != null && manager != null && !manager.isDestroyed()) {
            manager.beginTransaction()
                    .remove(mFragment)
                    .setTransition(FragmentTransaction.TRANSIT_NONE)
                    .commitAllowingStateLoss();
        }
    }

    private void addFragment(int viewId) {
        mHandler.post(() -> {
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(viewId, mFragment);
            transaction.commitAllowingStateLoss();
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play: {
                mWebView.loadUrl("javascript: window.vcontroller.play()");
                break;
            }
            case R.id.pause: {
                mWebView.loadUrl("javascript: window.vcontroller.pause()");
                break;
            }
        }
    }
}