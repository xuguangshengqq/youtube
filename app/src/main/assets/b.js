(function () {
    console.log("开始");
    function getSingleEleByClassName(className) {
        let ele = document.getElementsByClassName(className);
        if (ele != null && ele.length > 0) {
            ele = ele[0];
            console.log("ele = " , ele);
            return ele;
        }
        return null;
    }

    function removeSingleEleByClassName(className) {
        let ele = getSingleEleByClassName(className);
        console.log("class = ", className, " ele = ", ele);
        if (ele != null) ele.remove();
    }

    function notifyAppClientEent(event,arg) {
        if(!window.OpVideo) return;
        if(typeof(window.OpVideo[event])!="function") return;
        if (typeof(arg)!="undefined") {
            window.OpVideo[event](arg);
        } else {
            window.OpVideo[event]();
        }
    }

    function foo() {
        console.log(1);
        let thumb = getSingleEleByClassName("ytp-cued-thumbnail-overlay");
        console.log(thumb);
        if (thumb != null) {
            thumb.click();
            removeSingleEleByClassName("ytp-gradient-top");
            removeSingleEleByClassName("ytp-chrome-top ytp-show-cards-title");
            let video = getSingleEleByClassName("html5-main-video");
            console.log("video = " , video);
            if (video != null) {
                video.removeAttribute('controls');
                console.log("controls");
                video.onended = function(e) {
                    console.log("ended");
                    removeSingleEleByClassName("html5-endscreen");
                    removeSingleEleByClassName("ytp-icon-replay");
                    notifyAppClientEent("onEnded");
                };

                video.oncanplay = function(e) {
                    console.log("oncanplay");
                    notifyAppClientEent("onCanPlay");
                };

                video.canplaythrough = function(e) {
                    console.log("onCanPlayThrough");
                    notifyAppClientEent("onCanPlayThrough");
                };

                video.ondurationchange = function(e) {
                    console.log("onDurationChange");
                    notifyAppClientEent("onDurationChange",video.duration);
                };

                video.onemptied = function(e) {
                    console.log("onEmptied");
                    notifyAppClientEent("onEmptied");
                };

                video.onended = function(e) {
                    console.log("onEnded");
                    notifyAppClientEent("onEnded");
                };

                video.onerror = function(e) {
                    console.log("onError");
                    notifyAppClientEent("onError");
                };

                video.onloadeddata = function(e) {
                    console.log("onLoadedData");
                    notifyAppClientEent("onLoadedData");
                };

                video.onloadedmetadata = function(e) {
                    console.log("onLoadedMetaData");
                    notifyAppClientEent("onLoadedMetaData");
                };

                video.onloadstart = function(e) {
                    console.log("onLoadStart");
                    notifyAppClientEent("onLoadStart");
                };

                video.onpause = function(e) {
                    console.log("onPause");
                    notifyAppClientEent("onPause");
                };

                video.onplay = function(e) {
                    console.log("onPlay");
                    notifyAppClientEent("onPlay");
                };

                video.onplaying = function(e) {
                    console.log("onPlaying");
                    notifyAppClientEent("onPlaying");
                };

                video.onprogress = function(e) {
                    console.log("onProgress");
                    notifyAppClientEent("onProgress");
                };

                video.onratechange = function(e) {
                    console.log("onRatechange");
                    notifyAppClientEent("onRatechange");
                };

                video.onseeked = function(e) {
                    console.log("onSeeked");
                    notifyAppClientEent("onSeeked");
                };

                video.onstalled = function(e) {
                    console.log("onStalled");
                    notifyAppClientEent("onStalled");
                };

                video.onsuspend = function(e) {
                    console.log("onSuspend");
                    notifyAppClientEent("onSuspend");
                };

                video.ontimeupdate = function(e) {
                    console.log("onTimeUpdate");
                    notifyAppClientEent("onTimeUpdate", video.currentTime);
                };

                video.onvolumechange = function(e) {
                    console.log("onVolumeChange");
                    notifyAppClientEent("onVolumeChange");
                };

                video.onwaiting = function(e) {
                    console.log("onWaiting");
                    notifyAppClientEent("onWaiting");
                };
            }
        }
    }

    if ('complete' == document.readyState) {
        console.log(2);
        foo();
    }

    document.onreadystatechange = () => {
        if ('complete' == document.readyState) {
            console.log(3);
            foo();
        }
    };


    window.vcontroller = {
        video: document.getElementsByTagName("video")[0],
        seekTo(s) {
            console.log("seekTo ", s);
            this.video.currentTime = s;
        },
        play() {
            this.video.play();
        },
        
        pause() {
            this.video.pause();
        }
        ,
        retart() {
            this.seekTo(0);
            this.play();
        }
    };
    console.log("window.vcontroller:",window.vcontroller);
})();
