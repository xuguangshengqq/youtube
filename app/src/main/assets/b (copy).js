(function () {
    console.log("开始");
    function getSingleEleByClassName(className) {
        let ele = document.getElementsByClassName(className);
        if (ele != null && ele.length > 0) {
            ele = ele[0];
            console.log("ele = " , ele);
            return ele;
        }
        return null;
    }

    function removeSingleEleByClassName(className) {
        let ele = getSingleEleByClassName(className);
        console.log("class = ", className, " ele = ", ele);
        if (ele != null) ele.remove();
    }

    function notifyAppClientEent(event,arg) {
        if(!window.OpVideo) return;
        if(typeof(window.OpVideo[event])!="function") return;
        if (typeof(arg)!="undefined") {
            window.OpVideo[event](arg);
        } else{
            window.OpVideo[event]();
        }
    }

    function foo() {
        console.log(1);
        let thumb = getSingleEleByClassName("ytp-cued-thumbnail-overlay");
        console.log(thumb);
        if (thumb != null) {
            thumb.click();
            removeSingleEleByClassName("ytp-gradient-top");
            removeSingleEleByClassName("ytp-chrome-top ytp-show-cards-title");
            let video = getSingleEleByClassName("html5-main-video");
            console.log("video = " , video);
            if (video != null) {
                video.removeAttribute('controls');
                console.log("controls");
                video.onended = function(e) {
                    console.log("ended");
                    removeSingleEleByClassName("html5-endscreen");
                    removeSingleEleByClassName("ytp-icon-replay");
                    notifyAppClientEent("onEnded");
                }
                video.oncanplay = function(e) {
                    console.log("oncanplay");
                    notifyAppClientEent("onCanPlay");
                }
            }
        }
    }

    if ('complete' == document.readyState) {
        console.log(2);
        foo();
    }

    document.onreadystatechange = () => {
        if ('complete' == document.readyState) {
            console.log(3);
            foo();
        }
    };


    window.vcontroller = {
        video: document.getElementsByTagName("video")[0],
        seekTo(s) {
            console.log("seekTo ", s);
            this.video.currentTime = s;
        },
        start() {
            this.video.play();
        },
        retart() {
            this.seekTo(0);
            this.start();
        },
        pause() {
            this.video.pause();
        }
    };
    console.log("window.vcontroller:",window.vcontroller);
})();
